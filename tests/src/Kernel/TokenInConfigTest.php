<?php

namespace Drupal\Tests\token_in_config\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Token in config test class.
 *
 * @group token_in_config
 */
class TokenInConfigTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system'];

  /**
   * {@inheritdoc}
   */
  public function setUp() :void {
    parent::setUp();
    $this->container->get('config.factory')->getEditable("token_in_config.settings")->set('config_objects_and_keys', ['system.site|langcode'])->save();
  }

  /**
   * Tests tokens in config are replaced.
   */
  public function testTokenInConfig() {
    $token_service = \Drupal::service('token');
    // Use [site:url], a token with a value to avoid having to define one.
    $expectedValue = $token_service->replace("[site:url]");

    // Execute.
    $this->config('system.site')->set('langcode', "[site:url]")->save();
    $this->enableModules(['token_in_config']);

    // Assert.
    // Get immutable config so overrides are applied.
    $currentValue = $this->container->get('config.factory')->get('system.site')->get('langcode');
    $this->assertEquals($expectedValue, $currentValue);
  }

}
