<?php

namespace Drupal\token_in_config\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for token_in_config.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'token_in_config_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['token_in_config.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('token_in_config.settings');

    $configObjectsAndKeys = $config->get('config_objects_and_keys') ?: [];

    // Provide feedback in a table.
    $headers = [
      $this->t('Configuration object'),
      $this->t('Key'),
      $this->t('Original value'),
      $this->t('Current value'),
    ];

    $rows = [];
    foreach ($configObjectsAndKeys as $configObjectAndKey) {
      list($object, $key) = explode("|", $configObjectAndKey);
      $row['object'] = $object;
      $row['key'] = $key;
      $row['original'] = $this->configFactory->getEditable($object)->getOriginal($key, FALSE);
      $row['current'] = $this->configFactory->get($object)->get($key);
      $rows[] = $row;
    }

    $form['heading'] = [
      '#markup' => '<h2>' . $this->t("Configuration objects and keys that contain tokens") . '</h2>',
    ];

    $form['table'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => $this->t('No configuration objects and keys defined.'),
    ];

    $form['group'] = [
      '#type' => 'fieldset',
      '#title' => 'Configuration objects and keys',
      '#tree' => FALSE,
    ];
    $form['group']['prefix'] = [
      '#markup' => '<p>' . $this->t('Due to performance reasons, it is not possible to search for tokens in each and every one of the available configuration objects. Therefore, to narrow that search, it must be defined which configuration objects and keys contain tokens.') . '</p>',
    ];

    $form['group']['config_objects_and_keys'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Add configuration objects and keys that contain tokens'),
      '#required' => FALSE,
      '#description' => $this->t("One configuration object and key per line. It must define a configuration object, a pipe character, and a key, e.g.: <em>configuration.object|key</em>"),
      '#default_value' => implode("\n", $configObjectsAndKeys),

    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $configObjectsAndKeys = $form_state->getValue('config_objects_and_keys');
    if ($configObjectsAndKeys) {
      $configObjectsAndKeys = explode("\n", $configObjectsAndKeys);
      foreach ($configObjectsAndKeys as $line) {
        if (!preg_match("/^\S+\.\S+\|\S+/", $line)) {
          $form_state->setErrorByName(
            'config_objects_and_keys',
            $this->t('Line "@line" is not properly formed. Correct format is a configuration object, a pipe character, and a config key, e.g.: <em>configuration.object|key</em>.', ['@line' => $line])
          );
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('token_in_config.settings');
    $configObjectsAndKeys = explode("\n", $form_state->getValue('config_objects_and_keys'));
    $configObjectsAndKeys = array_map('trim', $configObjectsAndKeys);
    // Remove empty values.
    $configObjectsAndKeys = array_filter($configObjectsAndKeys);
    $config
      ->set('config_objects_and_keys', $configObjectsAndKeys)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
