# Token in config

## INTRODUCTION ##
Allows the use of tokens inside configuration values.

It allows the use of any token inside Drupal's configuration system. This is
useful when a config value depends on a value that cannot be hard coded in a
standard configuration object/key.

## REQUIREMENTS ##
No other module required.

## INSTALLATION ##
Run `composer require drupal/token_in_config`.

## CONFIGURATION ##
Due to performance reasons, it is not possible to search for tokens in each
and every one of the available configuration objects. Therefore, to narrow
that search, it must be defined which configuration objects and keys contain
tokens.

This is done at /admin/config/system/token-in-config

## Drupal 8 & 9 ##
 This module is compatible with Drupal 8 and Drupal 9.
