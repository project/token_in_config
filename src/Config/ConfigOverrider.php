<?php

namespace Drupal\token_in_config\Config;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\token_in_config\ProxyClass\Utility\Token;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Overrides config to give support for tokens.
 */
class ConfigOverrider implements ConfigFactoryOverrideInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The token system wrapped by a proxy class to avoid circular references.
   *
   * @var \Drupal\token_in_config\ProxyClass\Utility\Token
   */
  protected $tokenSystem;

  /**
   * Flag to indicate that $configObjectsAndKeys is loaded.
   *
   * @var bool
   */
  protected $configObjectsAndKeysLoaded;

  /**
   * Array of configuration objects and keys that contain tokens.
   *
   * @var array
   */
  protected $configObjectsAndKeys;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory, Token $token) {
    $this->configFactory = $configFactory;
    $this->tokenSystem = $token;
  }

  /**
   * Lazy load configuration objects and keys.
   */
  protected function loadConfigObjectsAndKeys() {
    $configObjectsAndKeys = $this->configFactory->getEditable('token_in_config.settings')->getOriginal('config_objects_and_keys', FALSE);
    if (is_array($configObjectsAndKeys)) {
      foreach ($configObjectsAndKeys as $config) {
        list($object, $key) = explode("|", $config);
        $this->configObjectsAndKeys[$object][$key] = TRUE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    $overrides = [];

    // Lazy load configuration objects and keys.
    if (!$this->configObjectsAndKeysLoaded) {
      $this->loadConfigObjectsAndKeys();
      $this->configObjectsAndKeysLoaded = TRUE;
    }

    if (!empty($this->configObjectsAndKeys)) {
      foreach ($names as $configObject) {
        if (!empty($this->configObjectsAndKeys[$configObject])) {
          foreach (array_keys($this->configObjectsAndKeys[$configObject]) as $key) {
            $original = $this->configFactory->getEditable($configObject)
              ->getOriginal($key, FALSE);
            if ($original) {
              $overrides[$configObject][$key] = $this->tokenSystem->replace($original);
            }
          }
        }
      }
    }

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'token_in_config';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
