<?php

namespace Drupal\token_in_config\Utility;

use Drupal\Core\Utility\Token as CoreToken;

/**
 * Wrapper around Drupal's Token system to use its ::replace() method.
 *
 * This wrapper is needed to load it lazily in order to avoid circular
 * references. There is also a Proxy Class located at
 * \Drupal\token_in_config\ProxyClass\Utility that is actually injected.
 */
class Token {
  /**
   * The token system.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenSystem;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Utility\Token $token_system
   *   Drupal's token system.
   */
  public function __construct(CoreToken $token_system) {
    $this->tokenSystem = $token_system;
  }

  /**
   * Wrapper around Token::replace method.
   *
   * @param string $text
   *   Text to be replaced.
   *
   * @return string
   *   The replaced string.
   */
  public function replace(string $text) {
    return $this->tokenSystem->replace($text);
  }

}
